package org.foc.angel.app.dao.impl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import org.foc.angel.app.object.dto.DTOTestCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

public class MongoDBDaoImplTest {

	DTOTestCollection test;

	MongoDBDaoImpl<String, DTOTestCollection> mongoImpl;

	private DBCollection collection;

	@Before
	public void setUp() throws IOException {
		test = new DTOTestCollection();
		mongoImpl = new MongoDBDaoImpl<String, DTOTestCollection>(test);
		MongoClient client = new MongoClient();
		DB db = client.getDB("telopaso");
		collection = db.getCollection("test");
	}

	@After
	public void tearDown() {
		DBCursor find = collection.find();
		while (find.hasNext()) {
			collection.remove(find.next());
		}
	}

	@Test
	public void test_obtain_all_collection_data() {
		test.setCadenaTest("hola");
		test.setIntTest(3);

		try {
			String json = test.toJSON();
			BasicDBObject parse = (BasicDBObject) JSON.parse(json);
			collection.insert(parse);
		} catch (UnknownHostException e) {
			fail("fallo al conectar");
		} catch (IOException e) {
			fail("fallo al serializar");
		}
		assertThat(mongoImpl.find().size(), is(1));
	}

	@Test
	public void test_obtain_zero_elements_in_collection() {
		assertThat(mongoImpl.find().size(), is(0));
	}

	@Test
	public void test_delete_element_in_collection() throws IOException {
		String prepareData = prepareData();
		mongoImpl.delete(prepareData);
		assertNull(collection.findOne());
	}

	@Test
	public void test_obtain_element_by_query() throws IOException {
		prepareData();

		assertNotNull(mongoImpl.findOneByQuery(test.toJSON()));
	}

	@Test
	public void test_obtain_data_with_json_query() {
		prepareData();
		Object[] parms = new Object[] { "papapp", 2 };
		List<DTOTestCollection> list = (List<DTOTestCollection>) mongoImpl.findJSONQuery(
				"queryParameter", parms);
		assertThat(list.size(), is(1));

	}

	private String prepareData() {
		test = new DTOTestCollection();
		test.setCadenaTest("papapp");
		test.setIntTest(2);
		return (String) mongoImpl.insert(test);
	}

}
