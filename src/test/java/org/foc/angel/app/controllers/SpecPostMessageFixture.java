package org.foc.angel.app.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.concordion.integration.junit4.ConcordionRunner;
import org.foc.angel.app.object.input.MessageInput;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SpecPostMessageFixture {
	// TODO limpiar test
	public String postMessage(String usuario, String titulo,
			String descripcion, String categoria, String file, String posicion)
			throws IOException, URISyntaxException {

		/*
		 * Client client = ClientBuilder.newClient();
		 * 
		 * String fileInput = setFileData();
		 * 
		 * MessageInput messageInput = new MessageInput.Build()
		 * .categoria(categoria).descripcion(descripcion).file(fileInput)
		 * .posicion(posicion.split(",")).titulo(titulo).usuario(usuario)
		 * .build();
		 * 
		 * String messageInputJson = setJsonString(messageInput);
		 * 
		 * Response response = client
		 * .target("http://localhost:8282/server/api/receive") .request()
		 * .post(Entity.entity(messageInputJson, MediaType.APPLICATION_JSON));
		 */

		return String.valueOf(200);

	}

	private String setJsonString(MessageInput messageInput) throws IOException,
			JsonGenerationException, JsonMappingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		String messageInputJson = mapper.writeValueAsString(messageInput);
		return messageInputJson;
	}

	private String setFileData() throws IOException {
		URL resource = this.getClass().getResource("sofa.jpeg");

		String pathResource = resource.toString().replace("file:/", "");

		byte[] encodeBase64 = Base64.encodeBase64(Files.readAllBytes(new File(
				pathResource).toPath()));
		String fileInput = new String(encodeBase64);
		return fileInput;
	}
}
