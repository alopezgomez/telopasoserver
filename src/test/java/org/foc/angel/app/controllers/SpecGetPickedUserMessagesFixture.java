package org.foc.angel.app.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.concordion.integration.junit4.ConcordionRunner;
import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.exceptions.MessageIsPickedException;
import org.foc.angel.app.exceptions.MessageNotExistsException;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.ReceiverService;
import org.foc.angel.app.services.impl.RececiverServiceImpl;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SpecGetPickedUserMessagesFixture {

	private ReceiverService receiverService = new RececiverServiceImpl();
	private ObjectMapper mapper = new ObjectMapper();
	private List<String> listSalida;
	private List<String> listDatosCreados = new ArrayList<>();

	public void setUpMessages(String message) throws IOException,
			MessageNotExistsException, MessageIsPickedException {
		MessageInput value = mapper.readValue(message, MessageInput.class);
		String idGenerado = receiverService.addMessage(value);
		MessageInput messagePicked = new MessageInput.Build().id(idGenerado)
				.usuario("angel").build();
		receiverService.markAsPicked(messagePicked);
		listDatosCreados.add(idGenerado);
	}

	public Iterable<String> getMessagesFromUser(String userName)
			throws IOException {
		listSalida = new ArrayList<>();

		Client client = ClientBuilder.newClient();

		String JSONResponse = client
				.target("http://localhost:8282/server/api/send/messages/picked")
				.path("{userName}").resolveTemplate("userName", userName)
				.request().get(String.class);

		parseResult(JSONResponse);

		return listSalida;
	}

	public void tearDownMessage() {
		MongoDBDaoBasic<Serializable, MongoDTOObject> mongo = new MongoDBDaoImpl<String, MessageDTO>(
				new MessageDTO());
		for (String id : listDatosCreados) {
			mongo.delete(id);
		}
	}

	private void parseResult(String response) throws IOException,
			JsonParseException, JsonMappingException, JsonGenerationException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		MessageInput[] array = new MessageInput[2];
		array = mapper.readValue(response, array.getClass());
		for (MessageInput resultMessage : array) {
			MessageInput mes = resultMessage;
			mes.setIdMessage(null);
			mes.setFeMessage(null);

			listSalida.add(mapper.writeValueAsString(mes));

		}

	}

}
