package org.foc.angel.app.controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.concordion.integration.junit4.ConcordionRunner;
import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SpecCheckMessageFixture {

	public Integer checkMessage(String posicion, String radio)
			throws IOException {

		Double longitud = Double.parseDouble(posicion.split(",")[0]);
		Double latidud = Double.parseDouble(posicion.split(",")[1]);
		Double[] posicionDouble = new Double[] { longitud, latidud };

		MessageDTO message = new MessageDTO.Builder().categoria("cat")
				.descripcion("desc").posicion(posicionDouble).titulo("tit")
				.usuario("user").idGenerado("999999").build();
		MongoDBDaoBasic<Serializable, MongoDTOObject> mongo = new MongoDBDaoImpl<String, MessageDTO>(
				new MessageDTO());
		mongo.insert(message);

		Client client = ClientBuilder.newClient();

		Integer response = client
				.target("http://localhost:8282/server/api/send")
				.queryParam("posicion", posicion).queryParam("radio", radio)
				.request().get(Integer.class);

		mongo.delete("999999");

		return response;
	}
}
