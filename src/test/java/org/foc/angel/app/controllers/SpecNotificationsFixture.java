package org.foc.angel.app.controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.concordion.integration.junit4.ConcordionRunner;
import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SpecNotificationsFixture {

	public String getMessages(String posicion, String radio, String user)
			throws IOException {

		MessageDTO message = prepareData(posicion);
		MongoDBDaoBasic<Serializable, MongoDTOObject> mongo = new MongoDBDaoImpl<String, MessageDTO>(
				new MessageDTO());
		mongo.insert(message);

		Client client = ClientBuilder.newClient();

		String response = client
				.target("http://localhost:8282/server/api/send/messages")
				.queryParam("posicion", posicion).queryParam("radio", radio)
				.queryParam("userName", user).request().get(String.class);
		mongo.delete("999999");

		String result = parseResult(response);

		return result;
	}

	private MessageDTO prepareData(String posicion) {
		Double longitud = Double.parseDouble(posicion.split(",")[0]);
		Double latidud = Double.parseDouble(posicion.split(",")[1]);
		Double[] posicionDouble = new Double[] { longitud, latidud };

		MessageDTO message = new MessageDTO.Builder().categoria("categoria")
				.descripcion("descripcion").posicion(posicionDouble)
				.titulo("titulo").usuario("user@gmail.com")
				.idGenerado("999999").build();
		return message;
	}

	private String parseResult(String response) throws IOException,
			JsonParseException, JsonMappingException, JsonGenerationException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		MessageInput[] array = new MessageInput[1];
		array = mapper.readValue(response, array.getClass());
		MessageInput mes = array[0];
		mes.setIdMessage(null);
		String result = mapper.writeValueAsString(mes);
		return result;
	}
}
