package org.foc.angel.app.controllers;

import java.io.IOException;
import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.concordion.integration.junit4.ConcordionRunner;
import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SpecObtainMessageFixture {

	public String getMessage(String idGenerado) throws IOException {

		MessageDTO data = prepareData("30,-30");
		data.setIdGenerado(idGenerado);
		MongoDBDaoBasic<Serializable, MongoDTOObject> mongo = new MongoDBDaoImpl<String, MessageDTO>(
				new MessageDTO());
		mongo.insert(data);

		Client client = ClientBuilder.newClient();

		String response = client
				.target("http://localhost:8282/server/api/send/messages")
				.path("{id}").resolveTemplate("id", idGenerado).request()
				.get(String.class);

		mongo.delete(idGenerado);

		return parseResult(response);

	}

	private MessageDTO prepareData(String posicion) {
		Double longitud = Double.parseDouble(posicion.split(",")[0]);
		Double latidud = Double.parseDouble(posicion.split(",")[1]);
		Double[] posicionDouble = new Double[] { longitud, latidud };

		MessageDTO message = new MessageDTO.Builder().categoria("categoria")
				.descripcion("descripcion").posicion(posicionDouble)
				.titulo("titulo").usuario("user@gmail.com").build();
		return message;
	}

	private String parseResult(String response) throws IOException,
			JsonParseException, JsonMappingException, JsonGenerationException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		MessageInput mes = mapper.readValue(response, MessageInput.class);
		mes.setIdMessage(null);
		String result = mapper.writeValueAsString(mes);
		return result;
	}

}
