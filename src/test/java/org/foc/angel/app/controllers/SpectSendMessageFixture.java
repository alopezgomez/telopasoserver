package org.foc.angel.app.controllers;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.runner.RunWith;

@RunWith(ConcordionRunner.class)
public class SpectSendMessageFixture {

	public Integer sendMessage(String usuario, String posicion, String radio)
			throws IOException {

		Client client = ClientBuilder.newClient();

		Integer response = client
				.target("http://localhost:8282/server/api/send/messages")
				.queryParam("position", posicion).queryParam("radio", radio)
				.request().get(Integer.class);

		return response;
	}
}
