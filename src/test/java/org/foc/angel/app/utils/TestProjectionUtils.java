package org.foc.angel.app.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.foc.angel.app.object.dto.MessageDTO;
import org.junit.Test;

import com.mongodb.BasicDBObject;

public class TestProjectionUtils {

	@Test
	public void projection_is_not_null_for_simple_example() {

		MessageDTO dto = new MessageDTO.Builder().usuario("asdas")
				.idGenerado("1").build();
		try {
			BasicDBObject makeProjectionForMongoDTOObject = ProjectionUtils
					.projection(dto);
			assertNotNull(makeProjectionForMongoDTOObject.get("usuario"));
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

}
