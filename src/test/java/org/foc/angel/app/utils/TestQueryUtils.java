package org.foc.angel.app.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.foc.angel.app.object.dto.DTOTestCollection;
import org.junit.Test;

public class TestQueryUtils {

	@Test
	public void querySimple_expected_in_annotation_is_returned() {

		String queryExpected = "{cadenaTest:'aaa'}";

		String queryReturned = QueryUtils.getJSONQuery("querySimple",
				DTOTestCollection.class, null);

		assertEquals(queryExpected, queryReturned);
	}

	@Test
	public void query_no_exists_in_annotation() {

		String queryReturned = QueryUtils.getJSONQuery("queryjjSimple",
				DTOTestCollection.class, null);

		assertNull(queryReturned);
	}

	@Test
	public void queryNoSimple_expected_in_annotation_is_returned() {

		String queryExpected = "{cadenaTest:'aaa',listUsers.userName:'pepe'}";

		String queryReturned = QueryUtils.getJSONQuery("queryNoSimple",
				DTOTestCollection.class, null);

		assertEquals(queryExpected, queryReturned);
	}

	@Test
	public void queryParameter_expected_in_annotation_is_returned() {

		String queryExpected = "{cadenaTest:'pepep',intTest:2}";

		String queryReturned = QueryUtils.getJSONQuery("queryParameter",
				DTOTestCollection.class, new Object[] { "pepep", 2 });

		assertEquals(queryExpected, queryReturned);

	}

}
