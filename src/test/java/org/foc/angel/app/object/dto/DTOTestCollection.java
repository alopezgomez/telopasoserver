package org.foc.angel.app.object.dto;

import java.io.IOException;
import java.io.Serializable;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.annotations.JSONQuery;
import org.foc.angel.app.annotations.JSONQuerys;
import org.foc.angel.app.object.MongoDTOObject;

@CollectionName("test")
@JSONQuerys({
		@JSONQuery(nameQuery = "querySimple", query = "{cadenaTest:'aaa'}"),
		@JSONQuery(nameQuery = "queryNoSimple", query = "{cadenaTest:'aaa',listUsers.userName:'pepe'}"),
		@JSONQuery(nameQuery = "queryParameter", query = "{cadenaTest:?,intTest:?}") })
public class DTOTestCollection implements MongoDTOObject, Serializable {

	/**
	 * 
	 */
	@JsonIgnore
	private static final long serialVersionUID = 8129684643650505348L;

	private String cadenaTest;
	private int intTest;
	private String idGenerado;
	private MongoID _id;

	public DTOTestCollection() {
	}

	public DTOTestCollection(String cadenaTest, int intTest) {
		this.cadenaTest = cadenaTest;
		this.intTest = intTest;
	}

	public String getCadenaTest() {
		return cadenaTest;
	}

	public void setCadenaTest(String cadenaTest) {
		this.cadenaTest = cadenaTest;
	}

	public int getIntTest() {
		return intTest;
	}

	public void setIntTest(int intTest) {
		this.intTest = intTest;
	}

	public MongoID get_id() {
		return _id;
	}

	public void set_id(MongoID _id) {
		this._id = _id;
	}

	@Override
	public String toJSON() throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);
	}

	@Override
	public DTOTestCollection toObject(String json) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, DTOTestCollection.class);
	}

	public String getIdGenerado() {
		return idGenerado;
	}

	public void setIdGenerado(String idGenerado) {
		this.idGenerado = idGenerado;
	}

}
