package org.foc.angel.app.services.impl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.SenderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.mongodb.BasicDBObject;

public class UnitTestSenderService {

	MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB;
	SenderService senderService;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {
		mongoDB = mock(MongoDBDaoExtendend.class);
		senderService = new SenderServiceImpl(mongoDB);
	}

	@Test
	public void is_called_findUsingProjection_when_execute_getNumberMessagesInPosition() {

		senderService.getNumberMessagesInPosition(setLocationData());

		verify(mongoDB).findUsingProjection(any(BasicDBObject.class),
				any(BasicDBObject.class));

	}

	@Test
	public void is_called_findUsingProjection_when_execute_getAllMessages() {

		senderService.getAllMessages(setLocationData());

		verify(mongoDB).findUsingProjection(any(BasicDBObject.class),
				any(BasicDBObject.class));

	}

	@Test
	public void when_find_any_location_then_return_list_not_empty() {

		Mockito.<List<? extends MongoDTOObject>> when(
				mongoDB.findUsingProjection(any(BasicDBObject.class),
						any(BasicDBObject.class))).thenReturn(geListData());
		List<MessageInput> listMessage = senderService
				.getAllMessages(setLocationData());

		assertThat(listMessage.isEmpty(), is(false));

	}

	@Test
	public void when_find_any_location_and_message_is_read_for_user_return_empty() {

		Mockito.<List<? extends MongoDTOObject>> when(
				mongoDB.findUsingProjection(any(BasicDBObject.class),
						any(BasicDBObject.class))).thenReturn(
				geListDataUsersRead());
		List<MessageInput> listMessage = senderService
				.getAllMessages(setLocationData());

		assertThat(listMessage.isEmpty(), is(true));

	}

	@Test
	public void when_find_any_location_and_message_is_picked_return_empty() {

		Mockito.<List<? extends MongoDTOObject>> when(
				mongoDB.findUsingProjection(any(BasicDBObject.class),
						any(BasicDBObject.class))).thenReturn(
				geListDataPicked());
		List<MessageInput> listMessage = senderService
				.getAllMessages(setLocationData());

		assertThat(listMessage.isEmpty(), is(true));

	}

	@Test
	public void is_called_findUsingProjection_when_exectute_getMessage() {

		Mockito.when(mongoDB.findOneByQuery(any(Serializable.class)))
				.thenReturn(
						new MessageDTO.Builder().categoria("cat")
								.descripcion("des").titulo("posicion").build());

		senderService.getMessage("1");
		verify(mongoDB).findOneByQuery(any(Serializable.class));

	}

	@Test
	public void is_called_findJSONQueryProjection_when_exectute_getUserMessages() {

		Mockito.<List<? extends MongoDTOObject>> when(
				mongoDB.findJSONQueryProjection(any(String.class),
						any(new Object[1].getClass()), any(BasicDBObject.class)))
				.thenReturn(geListData());

		senderService.getUserMessages("user");
		verify(mongoDB).findJSONQueryProjection(any(String.class),
				any(new Object[1].getClass()), any(BasicDBObject.class));

	}

	@Test
	public void when_find_any_message_users_return_not_empty() {
		Mockito.<List<? extends MongoDTOObject>> when(
				mongoDB.findJSONQueryProjection(any(String.class),
						any(new Object[1].getClass()), any(BasicDBObject.class)))
				.thenReturn(geListData());

		List<MessageInput> messages = senderService.getUserMessages("user");
		assertThat(messages.isEmpty(), is(false));
	}

	@Test
	public void is_called_findJSONQueryProjection_when_exectute_getPickedUserMessages() {

		Mockito.when(mongoDB.findOneByQuery(any(Serializable.class)))
				.thenReturn(
						new MessageDTO.Builder().categoria("cat")
								.descripcion("des").titulo("posicion").build());

		senderService.getPickedUserMessages("user");
		verify(mongoDB).findJSONQueryProjection(any(String.class),
				any(new Object[1].getClass()), any(BasicDBObject.class));
	}

	@Test
	public void when_find_any_message_picked_users_return_not_empty() {
		Mockito.<List<? extends MongoDTOObject>> when(
				mongoDB.findJSONQueryProjection(any(String.class),
						any(new Object[1].getClass()), any(BasicDBObject.class)))
				.thenReturn(geListDataPicked());

		List<MessageInput> messages = senderService
				.getPickedUserMessages("user");
		assertThat(messages.isEmpty(), is(false));
	}

	private List<? extends MongoDTOObject> geListData() {
		List<MongoDTOObject> listData = new ArrayList<>();
		MessageDTO dto = new MessageDTO.Builder().categoria("cat")
				.descripcion("des").titulo("posicion").build();
		listData.add(dto);
		return listData;
	}

	private List<? extends MongoDTOObject> geListDataUsersRead() {
		List<MongoDTOObject> listData = new ArrayList<>();
		MessageDTO dto = new MessageDTO.Builder().categoria("cat")
				.descripcion("des").titulo("posicion")
				.usersRead(new String[] { "angel" }).build();
		listData.add(dto);
		return listData;
	}

	private List<? extends MongoDTOObject> geListDataPicked() {
		List<MongoDTOObject> listData = new ArrayList<>();
		MessageDTO dto = new MessageDTO.Builder().categoria("cat")
				.descripcion("des").titulo("posicion").picked("user").build();
		listData.add(dto);
		return listData;
	}

	private LocationInput setLocationData() {
		return new LocationInput.Builder()
				.posicion(new String[] { "90", "-90" }).radio("4")
				.usuario("angel").build();
	}

}
