package org.foc.angel.app.services.impl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;

import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.dao.impl.MongoGridFSDaoImpl;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.ImageDTO;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.SenderService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ITTestSenderService {

	private LocationInput location;
	private SenderService senderService;
	private String idGenerado;
	private MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB;
	private MongoDBDaoBasic<Serializable, MongoDTOObject> daoImages;

	@Before
	public void setUp() throws IOException {
		mongoDB = new MongoDBDaoImpl<String, MessageDTO>(new MessageDTO());
		daoImages = new MongoGridFSDaoImpl<Serializable, MongoDTOObject>(
				new ImageDTO());
		MessageDTO messageDTO = new MessageDTO.Builder().categoria("categoria")
				.descripcion("descripcio").posicion(new Double[] { 25d, -25d })
				.titulo("titulo").usuario("user").build();

		idGenerado = (String) mongoDB.insert(messageDTO);

		location = new LocationInput.Builder()
				.posicion(new String[] { "25", "-25" }).radio("5").build();

		senderService = new SenderServiceImpl(mongoDB);
	}

	@After
	public void tearDown() {
		mongoDB.delete(idGenerado);
		daoImages.delete(idGenerado);
	}

	@Test
	public void location_is_find_with_params_input() {
		assertTrue(senderService.getNumberMessagesInPosition(location) > 0);
	}

	@Test
	public void return_messages_when_location_is_valid() {
		assertThat(senderService.getAllMessages(location).isEmpty(), is(false));

	}

	@Test
	public void return_message_without_content_file() {
		MessageInput message = senderService.getMessage(idGenerado);
		assertNotNull(message);
		assertNull(message.getFileUpload());
	}

	@Test
	public void return_message_with_content_file() throws IOException {
		ImageDTO imageDTO = new ImageDTO.Builder().content(setFileData())
				.contentType("image/jpeg").idMessage(idGenerado).build();

		daoImages.insert(imageDTO);

		MessageInput message = senderService.getMessage(idGenerado);
		assertNotNull(message.getFileUpload());
	}

	@Test
	public void return_list_of_users_messages() {
		assertThat(senderService.getUserMessages("user").size(), is(1));
	}

	@Test
	public void return_list_of_picked_users_messages() {
		MessageDTO messageDTO = new MessageDTO.Builder().categoria("categoria")
				.descripcion("descripcio").posicion(new Double[] { 25d, -25d })
				.titulo("titulo").picked("user").usuario("user").build();
		String idGenerado = (String) mongoDB.insert(messageDTO);
		assertThat(senderService.getPickedUserMessages("user").size(), is(1));
		mongoDB.delete(idGenerado);
	}

	private InputStream setFileData() throws IOException {
		URL resource = this.getClass().getResource(
				"../../../../../../sofa.jpeg");

		String pathResource = resource.toString().replace("file:/", "");

		ByteArrayInputStream inputStream = new ByteArrayInputStream(
				Files.readAllBytes(new File(pathResource).toPath()));

		return inputStream;
	}

}
