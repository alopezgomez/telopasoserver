package org.foc.angel.app.services.impl;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.Serializable;

import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.exceptions.MessageIsPickedException;
import org.foc.angel.app.exceptions.MessageNotExistsException;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.ReceiverService;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.BasicDBObject;

public class UnitTestReceiverService {

	private MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB;
	private ReceiverService receiverService;

	@Before
	public void setUp() {
		mongoDB = mock(MongoDBDaoExtendend.class);
		receiverService = new RececiverServiceImpl(mongoDB);
	}

	@Test
	public void is_called_save_method_when_execute_addMessage() {

		receiverService.addMessage(new MessageInput());

		verify(mongoDB).insert(any(MongoDTOObject.class));

	}

	@Test
	public void is_called_updateDataCollection_method_when_execute_markAsRead() {

		try {
			when(mongoDB.findOneByQuery(any(String.class))).thenReturn(
					new MessageDTO());
			receiverService.markAsRead(new MessageInput());
		} catch (MessageNotExistsException e) {
			fail();
		}

		verify(mongoDB).updateDataCollection(any(BasicDBObject.class),
				any(BasicDBObject.class));

	}

	@Test
	public void is_called_updateDataCollection_method_when_execute_markAsPicked()
			throws MessageIsPickedException {
		try {
			when(mongoDB.findOneByQuery(any(String.class))).thenReturn(
					new MessageDTO());
			receiverService.markAsPicked(new MessageInput());
		} catch (MessageNotExistsException e) {
			fail();
		}

		verify(mongoDB).updateDataCollection(any(BasicDBObject.class),
				any(BasicDBObject.class));
	}

	@Test(expected = MessageNotExistsException.class)
	public void when_call_markAsRead_with_not_exists_message_throw_MessageNotExistsException()
			throws MessageNotExistsException {
		when(mongoDB.findOneByQuery(any(String.class))).thenReturn(null);

		receiverService.markAsRead(new MessageInput());

	}

	@Test(expected = MessageNotExistsException.class)
	public void when_call_markAsPicked_with_not_exists_message_throw_MessageNotExistsException()
			throws MessageNotExistsException, MessageIsPickedException {
		when(mongoDB.findOneByQuery(any(String.class))).thenReturn(null);

		receiverService.markAsPicked(new MessageInput());
	}

	@Test(expected = MessageIsPickedException.class)
	public void when_call_markAsPicked_with_picked_message_throw_MessageIsPickedException()
			throws MessageNotExistsException, MessageIsPickedException {
		when(mongoDB.findOneByQuery(any(String.class))).thenReturn(
				new MessageDTO.Builder().picked("picked").build());

		receiverService.markAsPicked(new MessageInput());
	}
}
