package org.foc.angel.app.services.impl;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.exceptions.MessageIsPickedException;
import org.foc.angel.app.exceptions.MessageNotExistsException;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.ReceiverService;
import org.foc.angel.app.utils.Constantes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.BasicDBObject;

public class ITTestReceiverService {

	private static final String USER_FOR_TEST = "user@gmail.com";
	private MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB;
	private ReceiverService receiverService;

	@Before
	public void setUp() {
		mongoDB = new MongoDBDaoImpl<String, MessageDTO>(new MessageDTO());

		receiverService = new RececiverServiceImpl(mongoDB);
	}

	@After
	public void tearDown() {
		BasicDBObject query = new BasicDBObject("usuario", USER_FOR_TEST);
		BasicDBObject projection = new BasicDBObject("idGenerado",
				Constantes.VALUE_PROJECTION);

		List<MessageDTO> find = (List<MessageDTO>) mongoDB.findUsingProjection(
				query, projection);
		for (MessageDTO dto : find) {
			mongoDB.delete(dto.getIdGenerado());
		}
	}

	@Test
	public void message_is_inserted_without_file() {

		MessageInput input = new MessageInput.Build().categoria("categoria")
				.descripcion("descripcion").usuario(USER_FOR_TEST)
				.posicion(new String[] { "30", "-30" }).titulo("titulo")
				.build();

		receiverService.addMessage(input);

	}

	@Test
	public void message_is_inserted_with_file() throws IOException {

		MessageInput input = new MessageInput.Build().categoria("categoria")
				.descripcion("descripcion").usuario(USER_FOR_TEST)
				.posicion(new String[] { "30", "-30" }).titulo("titulo")
				.fileExtension(".jpeg").file(setFileData()).build();

		receiverService.addMessage(input);
	}

	@Test
	public void message_is_picked() throws MessageNotExistsException,
			MessageIsPickedException {

		String idGenerado = setDataForTest();

		receiverService.markAsPicked(new MessageInput.Build().id(idGenerado)
				.usuario("angel").build());

		MessageDTO message = checkMessage(idGenerado);

		assertEquals("angel", message.getIsPicked());

	}

	@Test
	public void message_is_user_read_by_one_user()
			throws MessageNotExistsException {
		String idGenerado = setDataForTest();
		receiverService.markAsRead(new MessageInput.Build().id(idGenerado)
				.usuario("angel").build());

		MessageDTO dto = checkMessage(idGenerado);

		assertThat(dto.getUsersRead().length, is(1));

	}

	@Test
	public void message_is_user_read_by_three_users()
			throws MessageNotExistsException {
		String idGenerado = setDataForTest();
		receiverService.markAsRead(new MessageInput.Build().id(idGenerado)
				.usuario("angel").build());
		receiverService.markAsRead(new MessageInput.Build().id(idGenerado)
				.usuario("pepe").build());
		receiverService.markAsRead(new MessageInput.Build().id(idGenerado)
				.usuario("ramon").build());

		MessageDTO dto = checkMessage(idGenerado);

		assertThat(dto.getUsersRead().length, is(3));

	}

	private MessageDTO checkMessage(String idGenerado) {
		BasicDBObject basicDBObject = new BasicDBObject("idGenerado",
				idGenerado);
		MessageDTO message = (MessageDTO) mongoDB.findOneByQuery(basicDBObject
				.toString());
		return message;
	}

	private String setDataForTest() {
		MessageDTO dto = new MessageDTO.Builder().categoria("cat")
				.descripcion("desce").titulo("titulo").usuario(USER_FOR_TEST)
				.build();

		String idGenereado = mongoDB.getSeqNumber(MessageDTO.class
				.getAnnotation(CollectionName.class).value());
		dto.setIdGenerado(idGenereado);
		mongoDB.insert(dto);
		return idGenereado;
	}

	private String setFileData() throws IOException {
		URL resource = this.getClass().getResource(
				"../../../../../../sofa.jpeg");

		String pathResource = resource.toString().replace("file:/", "");

		byte[] encodeBase64 = Base64.encodeBase64(Files.readAllBytes(new File(
				pathResource).toPath()));
		String fileInput = new String(encodeBase64);
		return fileInput;
	}

}
