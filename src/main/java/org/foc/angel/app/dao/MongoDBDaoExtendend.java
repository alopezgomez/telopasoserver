package org.foc.angel.app.dao;

import java.io.Serializable;
import java.util.List;

import org.foc.angel.app.annotations.JSONQuery;
import org.foc.angel.app.annotations.JSONQuerys;
import org.foc.angel.app.object.MongoDTOObject;

import com.mongodb.BasicDBObject;

/**
 * Interfaz generica para el manejo del almacenmiento de los DTO de la
 * aplicacion
 * 
 * @author angel
 * */
public interface MongoDBDaoExtendend<ID extends Serializable, DTO extends MongoDTOObject>
		extends MongoDBDaoBasic<Serializable, MongoDTOObject> {

	/**
	 * Metodo que realiza la busqueda a partir de una query y solamente muestra
	 * aquello que este en la proyeccion
	 * 
	 * @param query
	 * @param projection
	 * @return Lista de objetos de tipo {@link MongoDTOObject} resultado de la
	 *         query
	 * */
	List<? extends MongoDTOObject> findUsingProjection(BasicDBObject query,
			BasicDBObject projection);

	/**
	 * Metodo que realiza la busqueda a partir de una query definida con la
	 * anotacion {@link JSONQuery} o {@link JSONQuerys}
	 * 
	 * @param query
	 *            nombre de la query
	 * @param parms
	 *            objeto con los datos para realizar el filtro
	 * 
	 * @return Lista de objetos de tipo {@link MongoDTOObject} resultado de la
	 *         query
	 * */
	List<? extends MongoDTOObject> findJSONQuery(String query, Object[] parms);

	/**
	 * Metodo que realiza la busqueda a partir de una query definida con la
	 * anotacion {@link JSONQuery} o {@link JSONQuerys}
	 * 
	 * @param query
	 *            nombre de la query
	 * @param parms
	 *            objeto con los datos para realizar el filtro
	 * @param dto
	 * @return Lista de objetos de tipo {@link MongoDTOObject} resultado de la
	 *         query
	 * */
	List<? extends MongoDTOObject> findJSONQueryProjection(String query,
			Object[] parms, BasicDBObject dto);

	/**
	 * Metodo que obtiene el numero de secuencia generado para la identificacion
	 * interna de los datos de una coleccion
	 * 
	 * @param collectionName
	 *            nombre de la coleccion, este nombre debe de estar en la
	 *            coleccion secuences
	 * @return el secuencial generado
	 * */
	String getSeqNumber(String collectionName);

}
