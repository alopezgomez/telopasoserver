package org.foc.angel.app.dao.impl;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.utils.Constantes;
import org.foc.angel.app.utils.QueryUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/**
 * Implementacion de la interfaz {@link MongoDBDaoExtendend} para las
 * operaciones sobre las colecciones de la base de datos
 * */
public class MongoDBDaoImpl<ID extends Serializable, T extends MongoDTOObject>
		implements MongoDBDaoExtendend<Serializable, MongoDTOObject> {

	public ID key;
	public T mongoObject;
	private DBCollection collection;
	private DB db;
	private String collectionName;

	public MongoDBDaoImpl(T mongoObject) {

		this.mongoObject = mongoObject;
		this.collection = getCollection();
	}

	@Override
	public List<? extends MongoDTOObject> find() {

		DBCursor dbCursor = collection.find();
		return putResultIntoList(dbCursor);
	}

	@Override
	public MongoDTOObject findOneByQuery(Serializable queryString) {

		BasicDBObject basicDBObject = (BasicDBObject) JSON.parse(String
				.valueOf(queryString));

		DBCursor find = collection.find(basicDBObject);
		if (find.hasNext()) {
			DBObject dbObject = find.next();
			try {
				MongoDTOObject dtoObject = (MongoDTOObject) mongoObject
						.toObject(dbObject.toString());
				return dtoObject;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Serializable insert(MongoDTOObject mongoObject) {

		try {
			BasicDBObject objInsert = (BasicDBObject) JSON.parse(mongoObject
					.toJSON());

			collection.save(objInsert);

			return objInsert.getString(ATTR_ID_GENERADO);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getSeqNumber(String collectionName) {
		Object[] parms = new Object[] { collectionName };
		String querySeq = QueryUtils.getJSONQuery(Constantes.SEQUENCES,
				MongoDTOObject.class, parms);
		return (String) db.eval(querySeq);
	}

	@Override
	public void delete(Serializable key) {

		BasicDBObject objectDelete = new BasicDBObject();
		objectDelete.put(ATTR_ID_GENERADO, key);
		collection.remove(objectDelete);

	}

	@Override
	public void updateDataCollection(BasicDBObject criteria,
			BasicDBObject updated) {

		collection.update(criteria, updated, false, false);
	}

	private DBCollection getCollection() {
		CollectionName annotation = mongoObject.getClass().getAnnotation(
				CollectionName.class);
		collectionName = annotation.value();
		try {
			MongoClient mongoClient = new MongoClient();
			db = mongoClient.getDB(DB);
			return db.getCollection(collectionName);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<? extends MongoDTOObject> findUsingProjection(
			BasicDBObject query, BasicDBObject projection) {

		DBCursor dbCursor = collection.find(query, projection);

		return putResultIntoList(dbCursor);
	}

	private List<? extends MongoDTOObject> putResultIntoList(DBCursor dbCursor) {
		List<MongoDTOObject> arrayList = new ArrayList<MongoDTOObject>();
		while (dbCursor.hasNext()) {
			DBObject dbObject = dbCursor.next();
			try {
				MongoDTOObject object = (MongoDTOObject) mongoObject
						.toObject(dbObject.toString());
				arrayList.add(object);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return arrayList;
	}

	@Override
	public List<? extends MongoDTOObject> findJSONQuery(String query,
			Object[] parms) {

		String jsonQuery = getQueryParsed(query, parms);
		BasicDBObject basicDBObject = (BasicDBObject) JSON.parse(jsonQuery);
		DBCursor cursor = collection.find(basicDBObject);

		return putResultIntoList(cursor);
	}

	@Override
	public List<? extends MongoDTOObject> findJSONQueryProjection(String query,
			Object[] parms, BasicDBObject dto) {

		String queryParsed = getQueryParsed(query, parms);
		BasicDBObject queryBD = (BasicDBObject) JSON.parse(queryParsed);
		return findUsingProjection(queryBD, dto);
	}

	private String getQueryParsed(String query, Object[] parms) {
		String jsonQuery = QueryUtils.getJSONQuery(query,
				mongoObject.getClass(), parms);
		return jsonQuery;
	}

}
