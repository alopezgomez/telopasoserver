package org.foc.angel.app.dao.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.List;

import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.ImageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.mongodb.util.JSON;

/**
 * Implementacion de la interfaz {@link MongoDBDaoBasic} para el manejo de los
 * datos asociados a los ficheros binarios en la base de datos
 * */
public class MongoGridFSDaoImpl<ID extends Serializable, T extends MongoDTOObject>
		implements MongoDBDaoBasic<Serializable, MongoDTOObject> {

	public ID key;
	public T mongoObject;
	private GridFS gridFS;
	private DB db;
	private String collectionName;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public MongoGridFSDaoImpl(T mongoObject) {

		this.mongoObject = mongoObject;
		this.gridFS = getCollection();
	}

	@Override
	public List<? extends MongoDTOObject> find() {
		return null;
	}

	@Override
	public MongoDTOObject findOneByQuery(Serializable key) {
		ImageDTO imageDTO = null;
		BasicDBObject query = new BasicDBObject(ACCES_METADATA_ID, key);
		List<GridFSDBFile> files = gridFS.find(query);

		try {
			if (files != null && !files.isEmpty()) {
				imageDTO = new ImageDTO();
				GridFSDBFile gridFSDBFile = files.get(0);
				ByteArrayOutputStream contentOut = new ByteArrayOutputStream();
				gridFSDBFile.writeTo(contentOut);
				imageDTO.setContentOut(contentOut);
				imageDTO.setIdMessage((String) key);
				imageDTO.setContentType(gridFSDBFile.getContentType());
			}
		} catch (IOException e) {
			log.error("Error al escribir el contenido", e);
		}
		return imageDTO;
	}

	@Override
	public Serializable insert(MongoDTOObject mongoObject) {

		ImageDTO imageDTO = (ImageDTO) mongoObject;
		GridFSInputFile gridFSInputFile = gridFS.createFile(imageDTO
				.getContentIn());
		gridFSInputFile.setContentType(imageDTO.getContentType());
		try {
			gridFSInputFile.setMetaData((BasicDBObject) JSON.parse(mongoObject
					.toJSON()));
			gridFSInputFile.save();
		} catch (IOException e) {
			log.error("Error al guardar el fichero ", e);
		}

		return imageDTO.getIdMessage();
	}

	@Override
	public void delete(Serializable key) {
		BasicDBObject query = new BasicDBObject(ACCES_METADATA_ID, key);
		gridFS.remove(query);
	}

	@Override
	public void updateDataCollection(BasicDBObject criteria,
			BasicDBObject updated) {

	}

	private GridFS getCollection() {
		CollectionName annotation = mongoObject.getClass().getAnnotation(
				CollectionName.class);
		collectionName = annotation.value();
		try {
			MongoClient mongoClient = new MongoClient();
			db = mongoClient.getDB(DB);
			return new GridFS(db, collectionName);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

}
