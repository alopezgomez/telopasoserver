package org.foc.angel.app.dao;

import java.io.Serializable;
import java.util.List;

import org.foc.angel.app.object.MongoDTOObject;

import com.mongodb.BasicDBObject;

/**
 * Interfaz que define los metodos basicos para el tratamiento de los datos en
 * mongo db
 * */
public interface MongoDBDaoBasic<ID extends Serializable, DTO extends MongoDTOObject> {

	static final String DB = "telopaso";
	static final String ATTR_ID_GENERADO = "idGenerado";
	static final String ACCES_METADATA_ID = "metadata.idMessage";

	/**
	 * Metodo que retorna todos los datos de la coleccion
	 * 
	 * @return Lista de elementos de tipo {@link MongoDTOObject}
	 * */
	List<? extends MongoDTOObject> find();

	/**
	 * Metodo que retorna el elemento de tipo {@link MongoDTOObject} a partir de
	 * su clave
	 * 
	 * @param key
	 * @return Elemento de tipo {@link MongoDTOObject}
	 * */
	DTO findOneByQuery(ID key);

	/**
	 * Metodo que realiza el guardado del elemento retorna el identifiacador del
	 * elemento en la coleccion
	 * 
	 * @param mongoObject
	 * @return identifcador
	 * */
	ID insert(DTO mongoObject);

	/**
	 * Metodo que realiza la eliminacion de un elemento en la coleccion a partir
	 * de su clave
	 * 
	 * @param key
	 * */
	void delete(ID key);

	/**
	 * Metodo que realiza operaciones de actualizacion sobre una coleccion
	 * 
	 * @param criteria
	 * @param updated
	 * 
	 * 
	 * */
	void updateDataCollection(BasicDBObject criteria, BasicDBObject updated);

}
