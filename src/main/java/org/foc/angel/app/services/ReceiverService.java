package org.foc.angel.app.services;

import org.foc.angel.app.exceptions.MessageIsPickedException;
import org.foc.angel.app.exceptions.MessageNotExistsException;
import org.foc.angel.app.object.input.MessageInput;

/**
 * Interfaz que define las funcionalidades de para el controlador
 * {@link org.foc.angel.app.controllers.ReceiverServer}
 * 
 * @author angel
 * */
public interface ReceiverService {

	/**
	 * Metodo que a�ade un mensaje a la base de datos
	 * 
	 * @param messageInput
	 * @return TODO
	 * */
	public String addMessage(MessageInput messageInput);

	/**
	 * Metodo que marca el mensaje como leido para que al usuario no le vuelva a
	 * aparecer
	 * 
	 * @param messageInput
	 * @throws MessageNotExistsException
	 * */
	public void markAsRead(MessageInput messageInput)
			throws MessageNotExistsException;

	/**
	 * Metodo que marca el mensaje como cogido por un usuario, este mensaje no
	 * aparecera para los demas usuarios
	 * 
	 * @param messageInput
	 * @throws MessageNotExistsException
	 * @throws MessageIsPickedException
	 * */
	public void markAsPicked(MessageInput messageInput)
			throws MessageNotExistsException, MessageIsPickedException;

}
