package org.foc.angel.app.services;

import java.util.List;

import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.object.input.MessageInput;

/**
 * Interfaz que define las funcionalidades para el controlador
 * {@link org.foc.angel.app.controllers.SenderServer}
 * 
 * @author angel
 * **/
public interface SenderService {

	/**
	 * Metodo que retorna el numero de mensajes coincidentes a partir de una
	 * locaizacion
	 * 
	 * @param locationInput
	 *            objeto de tipo {@link LocationInput} que contiene los datos de
	 *            la localizacion
	 * @return {@link Integer} numero de mensajes en esa posicion
	 * */
	public Integer getNumberMessagesInPosition(LocationInput locationInput);

	/**
	 * Metodo que retorna los mensajes encontrados a partir de una localizacio
	 * 
	 * @param locationInput
	 *            objeto de tipo {@link LocationInput} que contiene los datos de
	 *            la localizacion
	 * @return Lista con los mensajes de tipo {@link MessageInput}
	 * */
	public List<MessageInput> getAllMessages(LocationInput locationInput);

	/**
	 * Metodo que retorna un mensaje concreto a partir de su identificador
	 * 
	 * @param idMessage
	 * @return objeto de tipo {@link MessageInput} con el contenido del mensaje
	 * */
	public MessageInput getMessage(String idMessage);

	/**
	 * Metodo que retorna la lista de objetos {@link MessageInput} con los
	 * mensajes de un usuario
	 * 
	 * @param userName
	 * @return Lista de mensajes por usuario
	 * */
	public List<MessageInput> getUserMessages(String userName);

	/**
	 * Metodo que retorna la lista de objetos {@link MessageInput} con los
	 * mensajes cogidos por un usuario
	 * 
	 * @param userName
	 * @return Lista de mensajes cogidos por un usuario
	 * */
	public List<MessageInput> getPickedUserMessages(String userName);
}
