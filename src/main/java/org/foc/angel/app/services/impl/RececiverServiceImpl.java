package org.foc.angel.app.services.impl;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.dao.impl.MongoGridFSDaoImpl;
import org.foc.angel.app.exceptions.MessageIsPickedException;
import org.foc.angel.app.exceptions.MessageNotExistsException;
import org.foc.angel.app.factory.Adapter;
import org.foc.angel.app.factory.AdaptersFactory;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.ImageDTO;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.ReceiverService;
import org.foc.angel.app.utils.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

@Named("receiverService")
public class RececiverServiceImpl implements ReceiverService {

	private MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public RececiverServiceImpl() {

		mongoDB = new MongoDBDaoImpl<String, MessageDTO>(new MessageDTO());
	}

	public RececiverServiceImpl(
			MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB) {
		this.mongoDB = mongoDB;
	}

	@Override
	public String addMessage(MessageInput messageInput) {
		Adapter adapter = AdaptersFactory.AdapterMessages.getAdapter();
		MessageDTO messageDTO = (MessageDTO) adapter
				.inputObjectToMongoDTO(messageInput);

		String seqNumber = mongoDB.getSeqNumber(MessageDTO.class.getAnnotation(
				CollectionName.class).value());

		messageDTO.setIdGenerado(seqNumber);

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		messageDTO.setFechaCreacion(df.format(today));

		MongoDBDaoBasic<Serializable, MongoDTOObject> daoImages = new MongoGridFSDaoImpl<String, ImageDTO>(
				new ImageDTO());

		if (StringUtils.isNotEmpty(messageInput.getFileUpload())) {
			Adapter imageAdapter = AdaptersFactory.AdapterImages.getAdapter();

			ImageDTO imageDTO = (ImageDTO) imageAdapter
					.inputObjectToMongoDTO(messageInput);
			imageDTO.setIdMessage(seqNumber);

			daoImages.insert(imageDTO);
		}

		mongoDB.insert(messageDTO);
		return seqNumber;

	}

	@Override
	public void markAsRead(MessageInput messageInput)
			throws MessageNotExistsException {

		Adapter messageAdapter = AdaptersFactory.AdapterMessages.getAdapter();
		MessageDTO messageDTO = (MessageDTO) messageAdapter
				.inputObjectToMongoDTO(messageInput);

		checkMessage(messageDTO);

		BasicDBObject basicDBObject = new BasicDBObject("idGenerado",
				messageDTO.getIdGenerado());

		BasicDBObject updated = new BasicDBObject(Constantes.OPERATOR_PUSH,
				new BasicDBObject("usersRead", messageDTO.getUsuario()));

		mongoDB.updateDataCollection(basicDBObject, updated);
	}

	@Override
	public void markAsPicked(MessageInput messageInput)
			throws MessageNotExistsException, MessageIsPickedException {
		Adapter messageAdapter = AdaptersFactory.AdapterMessages.getAdapter();
		MessageDTO messageDTO = (MessageDTO) messageAdapter
				.inputObjectToMongoDTO(messageInput);

		MessageDTO message = checkMessage(messageDTO);
		if (StringUtils.isNotBlank(message.getIsPicked())) {
			throw new MessageIsPickedException();
		}

		BasicDBObject basicDBObject = new BasicDBObject("idGenerado",
				messageDTO.getIdGenerado());

		message.setIsPicked(messageDTO.getUsuario());
		BasicDBObject updated;
		try {
			updated = (BasicDBObject) JSON.parse(message.toJSON());
			mongoDB.updateDataCollection(basicDBObject, updated);
		} catch (IOException e) {
			logger.error("Error al marcar como cogido ", e);
			throw new MessageNotExistsException(e);
		}

	}

	private MessageDTO checkMessage(MessageDTO messageDTO)
			throws MessageNotExistsException {
		try {
			String filter = new MessageDTO.Builder()
					.idGenerado(messageDTO.getIdGenerado()).build().toJSON();
			MessageDTO messageExists = (MessageDTO) mongoDB
					.findOneByQuery(filter);
			if (messageExists == null) {
				throw new MessageNotExistsException();
			}
			return messageExists;
		} catch (IOException e) {
			logger.error("Error en checkMessage ", e);
			throw new MessageNotExistsException(e);
		}
	}

}
