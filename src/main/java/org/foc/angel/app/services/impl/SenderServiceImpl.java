package org.foc.angel.app.services.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.foc.angel.app.dao.MongoDBDaoBasic;
import org.foc.angel.app.dao.MongoDBDaoExtendend;
import org.foc.angel.app.dao.impl.MongoDBDaoImpl;
import org.foc.angel.app.dao.impl.MongoGridFSDaoImpl;
import org.foc.angel.app.factory.Adapter;
import org.foc.angel.app.factory.AdaptersFactory;
import org.foc.angel.app.helpers.SenderServiceHelper;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.ImageDTO;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.SenderService;
import org.foc.angel.app.utils.Constantes;
import org.foc.angel.app.utils.QueryUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

@Named("senderService")
public class SenderServiceImpl implements SenderService {

	private MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private SenderServiceHelper helper = new SenderServiceHelper();

	public SenderServiceImpl() {
		mongoDB = new MongoDBDaoImpl<String, MessageDTO>(new MessageDTO());
	}

	public SenderServiceImpl(
			MongoDBDaoExtendend<Serializable, MongoDTOObject> mongoDB) {
		this.mongoDB = mongoDB;
	}

	@Override
	public Integer getNumberMessagesInPosition(LocationInput locationInput) {
		BasicDBObject projection;
		try {
			projection = helper.prepareProjection();
			List<MessageDTO> listResult = executeGetMessages(locationInput,
					projection);
			List<MessageInput> result = helper.transformResult(listResult,
					locationInput.getUsuario());

			return result.size();
		} catch (Exception e) {
			logger.error("Error en getNumberMessagesInPosition", e);

		}
		return 0;
	}

	@Override
	public List<MessageInput> getAllMessages(LocationInput locationInput) {
		try {
			BasicDBObject projection = helper.prepareProjectionAllMessages();
			List<MessageDTO> listResult = executeGetMessages(locationInput,
					projection);
			return helper.transformResult(listResult,
					locationInput.getUsuario());
		} catch (Exception e) {
			logger.error("Error en getAllMessages", e);
		}

		return new ArrayList<>();
	}

	@Override
	public MessageInput getMessage(String idMessage) {
		String query = QueryUtils.getJSONQuery(Constantes.QUERY_MESSAGES,
				MessageDTO.class, new Object[] { idMessage });
		MessageDTO messageDTO = (MessageDTO) mongoDB.findOneByQuery(query);
		if (messageDTO != null) {
			Adapter messageAdapter = AdaptersFactory.AdapterMessages
					.getAdapter();

			MessageInput input = (MessageInput) messageAdapter
					.mongoDTOToInputObject(messageDTO);

			MongoDBDaoBasic<Serializable, MongoDTOObject> daoImages = new MongoGridFSDaoImpl<String, ImageDTO>(
					new ImageDTO());

			ImageDTO imageDTO = (ImageDTO) daoImages.findOneByQuery(idMessage);

			if (imageDTO != null) {
				setDataImage(input, imageDTO);
			}
			return input;
		}
		return new MessageInput();

	}

	private void setDataImage(MessageInput input, ImageDTO imageDTO) {
		Adapter imageAdapter = AdaptersFactory.AdapterImages.getAdapter();
		MessageInput messageInputContent = (MessageInput) imageAdapter
				.mongoDTOToInputObject(imageDTO);
		input.setFileExtension(messageInputContent.getFileExtension());
		input.setFileUpload(messageInputContent.getFileUpload());
	}

	@Override
	public List<MessageInput> getUserMessages(String userName) {
		List<MessageInput> listMessages = new ArrayList<>();
		try {
			String query = Constantes.QUERY_USER;
			List<MessageDTO> listResult = getDataQuery(userName, query);
			return helper.transformResult(listResult, userName);
		} catch (Exception e) {
			logger.error("Error al realizar la consulta en getUserMessages", e);
		}

		return listMessages;
	}

	@Override
	public List<MessageInput> getPickedUserMessages(String userName) {
		List<MessageInput> listMessages = new ArrayList<>();
		try {
			String query = Constantes.QUERY_PICKED;
			List<MessageDTO> listResult = getDataQuery(userName, query);
			return helper.transformResultForPicked(listResult, userName);

		} catch (Exception e) {
			logger.error(
					"Error al realizar la consulta en getPickedUserMessages", e);
		}
		return listMessages;
	}

	private List<MessageDTO> executeGetMessages(LocationInput locationInput,
			BasicDBObject projection) {
		String jsonQuery = helper.prepareQuery(locationInput);
		@SuppressWarnings("unchecked")
		List<MessageDTO> listResult = (List<MessageDTO>) mongoDB
				.findUsingProjection((BasicDBObject) JSON.parse(jsonQuery),
						projection);
		return listResult;
	}

	private List<MessageDTO> getDataQuery(String userName, String query)
			throws Exception {
		BasicDBObject projectionAllMessages = helper
				.prepareProjectionAllMessages();
		List<MessageDTO> listResult = (List<MessageDTO>) mongoDB
				.findJSONQueryProjection(query, new Object[] { userName },
						projectionAllMessages);
		return listResult;
	}
}