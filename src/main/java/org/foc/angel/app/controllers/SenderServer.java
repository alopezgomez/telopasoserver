package org.foc.angel.app.controllers;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.SenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servicio Web Rest que actua como receptor y emisor de las peticiones
 * generadas en cliente sobre el negocio de la aplicacion
 * */
@Path("/send")
public class SenderServer {

	private static final String MSG_ERROR_GENERAR_JSON_SALIDA = "Error al generar el json de salida";

	@Inject
	private SenderService senderService;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@GET
	public Response checkMessages(@QueryParam("posicion") String posicion,
			@QueryParam("radio") String radio,
			@QueryParam("userName") String userName) {

		LocationInput locationInput = loadLocation(posicion, radio, userName);

		Integer numberMessagesInPosition = senderService
				.getNumberMessagesInPosition(locationInput);

		return Response.ok(numberMessagesInPosition).build();
	}

	@GET
	@Path("/messages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessages(@QueryParam("posicion") String posicion,
			@QueryParam("radio") String radio,
			@QueryParam("userName") String userName) {

		LocationInput locationInput = loadLocation(posicion, radio, userName);

		List<MessageInput> allMessages = senderService
				.getAllMessages(locationInput);

		return serializeResponse(allMessages.toArray());
	}

	@GET
	@Path("/messages/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessage(@PathParam("id") String idMessage) {

		MessageInput message = senderService.getMessage(idMessage);
		return serializeResponse(message);
	}

	@GET
	@Path("/messages/users/{userName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserMessages(@PathParam("userName") String userName) {

		List<MessageInput> listMessagesUser = senderService
				.getUserMessages(userName);

		return serializeResponse(listMessagesUser.toArray());
	}

	@GET
	@Path("/messages/picked/{userName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPickedUserMessages(@PathParam("userName") String userName) {

		List<MessageInput> listMessagesPicked = senderService
				.getPickedUserMessages(userName);

		return serializeResponse(listMessagesPicked);
	}

	private Response serializeResponse(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonSalida = mapper.writeValueAsString(object);
			return Response.ok(jsonSalida).build();
		} catch (IOException e) {
			log.error(MSG_ERROR_GENERAR_JSON_SALIDA, e);
			return Response.serverError().entity(MSG_ERROR_GENERAR_JSON_SALIDA)
					.build();
		}
	}

	private LocationInput loadLocation(String posicion, String radio,
			String userName) {
		LocationInput locationInput = new LocationInput.Builder()
				.posicion(posicion.split(",")).radio(radio).usuario(userName)
				.build();
		return locationInput;
	}
}
