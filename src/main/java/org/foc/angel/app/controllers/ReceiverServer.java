package org.foc.angel.app.controllers;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.foc.angel.app.exceptions.MessageIsPickedException;
import org.foc.angel.app.exceptions.MessageNotExistsException;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.services.ReceiverService;

/**
 * Servicio web Rest expuesto para la recepcion de mensajes al servidor
 * */
@Path("/receive")
public class ReceiverServer {

	@Inject
	private ReceiverService receiverService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postMessage(MessageInput messageInput) {

		receiverService.addMessage(messageInput);

		return Response.ok().build();
	}

	@PUT
	@Path("/pick/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response pickMessage(@PathParam("id") String idMessage,
			String usuario) {

		try {
			receiverService.markAsPicked(new MessageInput.Build().id(idMessage)
					.usuario(usuario).build());
			return Response.ok().build();
		} catch (MessageNotExistsException | MessageIsPickedException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}

	}

	@PUT
	@Path("/read/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response readMessage(@PathParam("id") String idMessage,
			String usuario) {

		try {
			receiverService.markAsRead(new MessageInput.Build().id(idMessage)
					.usuario(usuario).build());
			return Response.ok().build();
		} catch (MessageNotExistsException e) {
			return Response.serverError().build();
		}

	}

}
