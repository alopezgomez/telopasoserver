package org.foc.angel.app.object.dto;

import java.io.Serializable;

/**
 * Objeto de tipo mongo id para la key de la coleccion
 * */
public class MongoID implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3311051354554340707L;
	private String $oid;

	public String get$oid() {
		return $oid;
	}

	public void set$oid(String $oid) {
		this.$oid = $oid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (($oid == null) ? 0 : $oid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MongoID other = (MongoID) obj;
		if ($oid == null) {
			if (other.$oid != null) {
				return false;
			}
		} else if (!$oid.equals(other.$oid)) {
			return false;
		}
		return true;
	}

}
