package org.foc.angel.app.object.input;

import java.io.Serializable;
import java.util.Arrays;

import org.foc.angel.app.object.InputObject;

/**
 * Objeto de tipo {@link InputObject} que contiene los datos de entrada de la
 * localizacion
 * */
public class LocationInput implements Serializable, InputObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7656937385909757025L;

	private String usuario;
	private String[] posicion;
	private String radio;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String[] getPosicion() {
		return posicion;
	}

	public void setPosicion(String[] posicion) {
		this.posicion = posicion;
	}

	public String getRadio() {
		return radio;
	}

	public void setRadio(String radio) {
		this.radio = radio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(posicion);
		result = prime * result + ((radio == null) ? 0 : radio.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LocationInput other = (LocationInput) obj;
		if (!Arrays.equals(posicion, other.posicion)) {
			return false;
		}
		if (radio == null) {
			if (other.radio != null) {
				return false;
			}
		} else if (!radio.equals(other.radio)) {
			return false;
		}
		if (usuario == null) {
			if (other.usuario != null) {
				return false;
			}
		} else if (!usuario.equals(other.usuario)) {
			return false;
		}
		return true;
	}

	public static class Builder {

		private String usuario;
		private String[] posicion;
		private String radio;

		public Builder usuario(String usuario) {
			this.usuario = usuario;
			return this;
		}

		public Builder posicion(String[] posicion) {
			this.posicion = posicion;
			return this;
		}

		public Builder radio(String radio) {
			this.radio = radio;
			return this;
		}

		public LocationInput build() {
			LocationInput locationInput = new LocationInput();
			locationInput.setPosicion(posicion);
			locationInput.setRadio(radio);
			locationInput.setUsuario(usuario);
			return locationInput;
		}
	}
}