package org.foc.angel.app.object;

import java.io.IOException;

import org.foc.angel.app.annotations.JSONQuery;
import org.foc.angel.app.utils.Constantes;

/**
 * Interfaz contrato para los objetos de persistencia en MongoDB
 * 
 * @author angel
 * */
@JSONQuery(nameQuery = Constantes.SEQUENCES, query = "function getNextVal(){var ret= db.sequences.findAndModify({"
		+ "	query : {_id: ? },"
		+ "	update :{ $inc:{ seq:1} },"
		+ "	new : true"
		+ "});" + "return ret.seq.toString();}")
public interface MongoDTOObject {

	/**
	 * Metodo que realiza la conversion a String en formato JSON
	 * 
	 * @return cadena en formato JSON
	 * @throws IOException
	 * */
	public String toJSON() throws IOException;

	/**
	 * Metodo que realiza la conversion a objeto a partir de una cadena JSON
	 * 
	 * @param JSONOrigen
	 *            cadena json a convertir
	 * @return objeto de tipo {@link InputObject}
	 * @throws IOException
	 * */
	public MongoDTOObject toObject(String JSONOrigen) throws IOException;

}
