package org.foc.angel.app.object.dto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.object.MongoDTOObject;

/**
 * Objeto de tipo {@link MongoDTOObject} que contiene los datos de las imagenes
 * */
@CollectionName("images")
public class ImageDTO implements Serializable, MongoDTOObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8517035629803952377L;
	private MongoID _id;
	private String idMessage;
	@JsonIgnore
	private String contentType;
	@JsonIgnore
	private InputStream contentIn;
	@JsonIgnore
	private ByteArrayOutputStream contentOut;

	public MongoID get_id() {
		return _id;
	}

	public void set_id(MongoID _id) {
		this._id = _id;
	}

	public String getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getContentIn() {
		return contentIn;
	}

	public void setContentIn(InputStream contentIn) {
		this.contentIn = contentIn;
	}

	public ByteArrayOutputStream getContentOut() {
		return contentOut;
	}

	public void setContentOut(OutputStream contentOut) {
		this.contentOut = (ByteArrayOutputStream) contentOut;
	}

	@Override
	public String toJSON() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);
	}

	@Override
	public MongoDTOObject toObject(String JSONOrigen) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.readValue(JSONOrigen, ImageDTO.class);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result
				+ ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result
				+ ((idMessage == null) ? 0 : idMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ImageDTO other = (ImageDTO) obj;
		if (_id == null) {
			if (other._id != null) {
				return false;
			}
		} else if (!_id.equals(other._id)) {
			return false;
		}
		if (contentType == null) {
			if (other.contentType != null) {
				return false;
			}
		} else if (!contentType.equals(other.contentType)) {
			return false;
		}
		if (idMessage == null) {
			if (other.idMessage != null) {
				return false;
			}
		} else if (!idMessage.equals(other.idMessage)) {
			return false;
		}
		return true;
	}

	public static class Builder {
		private MongoID _id;
		private String idMessage;
		private String contentType;
		private InputStream contentIn;
		private OutputStream contentOut;

		public Builder _id(MongoID _id) {
			this._id = _id;
			return this;
		}

		public Builder idMessage(String idMessage) {
			this.idMessage = idMessage;
			return this;
		}

		public Builder contentType(String contentType) {
			this.contentType = contentType;
			return this;
		}

		public Builder content(InputStream contentIn) {
			this.contentIn = contentIn;
			return this;
		}

		public Builder contentOut(OutputStream contentOut) {
			this.contentOut = contentOut;
			return this;
		}

		public ImageDTO build() {

			ImageDTO imageDTO = new ImageDTO();
			imageDTO.set_id(_id);
			imageDTO.setContentIn(contentIn);
			imageDTO.setContentType(contentType);
			imageDTO.setContentOut(contentOut);
			imageDTO.setIdMessage(idMessage);
			return imageDTO;

		}

	}

}
