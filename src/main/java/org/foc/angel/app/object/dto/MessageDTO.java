package org.foc.angel.app.object.dto;

import java.io.IOException;
import java.util.Arrays;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.foc.angel.app.annotations.CollectionName;
import org.foc.angel.app.annotations.JSONQuery;
import org.foc.angel.app.annotations.JSONQuerys;
import org.foc.angel.app.annotations.Projection;
import org.foc.angel.app.enums.ProyectionType;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.utils.Constantes;

/**
 * Clase que contiene los datos de la coleccion messages
 * 
 * @author angel
 * **/
@CollectionName("messages")
@JSONQuerys({
		@JSONQuery(nameQuery = Constantes.QUERY_MESSAGES, query = "{idGenerado:?}"),
		@JSONQuery(nameQuery = Constantes.QUERY_USER, query = "{usuario:?}"),
		@JSONQuery(nameQuery = Constantes.QUERY_PICKED, query = "{isPicked:?}") })
public class MessageDTO implements MongoDTOObject {

	private MongoID _id;
	private String idGenerado;
	private String usuario;
	private String titulo;
	private String descripcion;
	private String categoria;
	private Double[] posicion;
	private String[] usersRead;
	private String picked;
	private String fechaCreacion;

	public MongoID get_id() {
		return _id;
	}

	public void set_id(MongoID _id) {
		this._id = _id;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getIdGenerado() {
		return idGenerado;
	}

	public void setIdGenerado(String idGenerado) {
		this.idGenerado = idGenerado;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Projection(ProyectionType.SIMPLE)
	public Double[] getPosicion() {
		return posicion;
	}

	public void setPosicion(Double[] posicion) {
		this.posicion = posicion;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getIsPicked() {
		return picked;
	}

	public void setIsPicked(String isPicked) {
		this.picked = isPicked;
	}

	@Projection(ProyectionType.SIMPLE)
	public String[] getUsersRead() {
		return usersRead;
	}

	public void setUsersRead(String[] usersRead) {
		this.usersRead = usersRead;
	}

	@Projection(ProyectionType.SIMPLE)
	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result
				+ ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result
				+ ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime * result
				+ ((idGenerado == null) ? 0 : idGenerado.hashCode());
		result = prime * result + Arrays.hashCode(posicion);
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MessageDTO other = (MessageDTO) obj;
		if (categoria == null) {
			if (other.categoria != null) {
				return false;
			}
		} else if (!categoria.equals(other.categoria)) {
			return false;
		}
		if (descripcion == null) {
			if (other.descripcion != null) {
				return false;
			}
		} else if (!descripcion.equals(other.descripcion)) {
			return false;
		}
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null) {
				return false;
			}
		} else if (!fechaCreacion.equals(other.fechaCreacion)) {
			return false;
		}
		if (idGenerado == null) {
			if (other.idGenerado != null) {
				return false;
			}
		} else if (!idGenerado.equals(other.idGenerado)) {
			return false;
		}
		if (!Arrays.equals(posicion, other.posicion)) {
			return false;
		}
		if (titulo == null) {
			if (other.titulo != null) {
				return false;
			}
		} else if (!titulo.equals(other.titulo)) {
			return false;
		}
		return true;
	}

	@Override
	public String toJSON() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.writeValueAsString(this);
	}

	@Override
	public MongoDTOObject toObject(String JSONOrigen) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Inclusion.NON_NULL);
		return mapper.readValue(JSONOrigen, MessageDTO.class);
	}

	public static class Builder {

		private MongoID _id;
		private String idGenerado;
		private String usuario;
		private String titulo;
		private String descripcion;
		private String categoria;
		private Double[] posicion;
		private String[] usersRead;
		private String isPicked;
		private String feCreacion;

		public Builder _id(MongoID _id) {
			this._id = _id;
			return this;
		}

		public Builder idGenerado(String idGenerado) {
			this.idGenerado = idGenerado;
			return this;
		}

		public Builder usuario(String usuario) {
			this.usuario = usuario;
			return this;
		}

		public Builder titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public Builder descripcion(String descripcion) {
			this.descripcion = descripcion;
			return this;
		}

		public Builder categoria(String categoria) {
			this.categoria = categoria;
			return this;
		}

		public Builder posicion(Double[] posicion) {
			this.posicion = posicion;
			return this;
		}

		public Builder usersRead(String[] usersRead) {
			this.usersRead = usersRead;
			return this;
		}

		public Builder picked(String picked) {
			this.isPicked = picked;
			return this;
		}

		public Builder feCreacion(String feCreacion) {
			this.feCreacion = feCreacion;
			return this;
		}

		public MessageDTO build() {
			MessageDTO messageDTO = new MessageDTO();
			messageDTO.set_id(_id);
			messageDTO.setCategoria(categoria);
			messageDTO.setDescripcion(descripcion);
			messageDTO.setIdGenerado(idGenerado);
			messageDTO.setPosicion(posicion);
			messageDTO.setTitulo(titulo);
			messageDTO.setUsuario(usuario);
			messageDTO.setIsPicked(isPicked);
			messageDTO.setUsersRead(usersRead);
			messageDTO.setFechaCreacion(feCreacion);
			return messageDTO;
		}
	}
}
