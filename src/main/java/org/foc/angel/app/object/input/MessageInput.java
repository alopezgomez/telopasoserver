package org.foc.angel.app.object.input;

import java.io.Serializable;
import java.util.Arrays;

import org.foc.angel.app.object.InputObject;

/**
 * Objeto que recoge los datos de un mensaje enviado desde la aplicacion cliente
 * 
 * @author angel
 * 
 * */
public class MessageInput implements Serializable, InputObject {

	private static final long serialVersionUID = -909428071980425348L;

	private String idMessage;
	private String usuario;
	private String titulo;
	private String descripcion;
	private String categoria;
	private String fileUpload;
	private String fileExtension;
	private String[] posicion;
	private String feMessage;

	public String getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(String fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String[] getPosicion() {
		return posicion;
	}

	public void setPosicion(String[] posicion) {
		this.posicion = posicion;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFeMessage() {
		return feMessage;
	}

	public void setFeMessage(String feMessage) {
		this.feMessage = feMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result
				+ ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result
				+ ((fileExtension == null) ? 0 : fileExtension.hashCode());
		result = prime * result
				+ ((fileUpload == null) ? 0 : fileUpload.hashCode());
		result = prime * result
				+ ((idMessage == null) ? 0 : idMessage.hashCode());
		result = prime * result + Arrays.hashCode(posicion);
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MessageInput other = (MessageInput) obj;
		if (categoria == null) {
			if (other.categoria != null) {
				return false;
			}
		} else if (!categoria.equals(other.categoria)) {
			return false;
		}
		if (descripcion == null) {
			if (other.descripcion != null) {
				return false;
			}
		} else if (!descripcion.equals(other.descripcion)) {
			return false;
		}
		if (fileExtension == null) {
			if (other.fileExtension != null) {
				return false;
			}
		} else if (!fileExtension.equals(other.fileExtension)) {
			return false;
		}
		if (fileUpload == null) {
			if (other.fileUpload != null) {
				return false;
			}
		} else if (!fileUpload.equals(other.fileUpload)) {
			return false;
		}
		if (idMessage == null) {
			if (other.idMessage != null) {
				return false;
			}
		} else if (!idMessage.equals(other.idMessage)) {
			return false;
		}
		if (!Arrays.equals(posicion, other.posicion)) {
			return false;
		}
		if (titulo == null) {
			if (other.titulo != null) {
				return false;
			}
		} else if (!titulo.equals(other.titulo)) {
			return false;
		}
		if (usuario == null) {
			if (other.usuario != null) {
				return false;
			}
		} else if (!usuario.equals(other.usuario)) {
			return false;
		}
		return true;
	}

	/**
	 * Clase anidada que implementa el patr�n builder para la construccion del
	 * objeto {@link MessageInput}
	 * */
	public static class Build {
		private String idMessage;
		private String usuario;
		private String titulo;
		private String descripcion;
		private String categoria;
		private String fileUpload;
		private String[] posicion;
		private String fileExtension;
		private String feMessage;

		public Build id(String idMessage) {
			this.idMessage = idMessage;
			return this;
		}

		public Build usuario(String usuario) {
			this.usuario = usuario;
			return this;
		}

		public Build titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public Build descripcion(String descripcion) {
			this.descripcion = descripcion;
			return this;
		}

		public Build categoria(String categoria) {
			this.categoria = categoria;
			return this;
		}

		public Build file(String fileUpload) {
			this.fileUpload = fileUpload;
			return this;
		}

		public Build posicion(String[] posicion) {
			this.posicion = posicion;
			return this;
		}

		public Build fileExtension(String fileExtension) {
			this.fileExtension = fileExtension;
			return this;
		}

		public Build date(String feMessage) {
			this.feMessage = feMessage;
			return this;
		}

		public MessageInput build() {
			MessageInput input = new MessageInput();
			input.setIdMessage(idMessage);
			input.setCategoria(categoria);
			input.setDescripcion(descripcion);
			input.setFileUpload(fileUpload);
			input.setPosicion(posicion);
			input.setTitulo(titulo);
			input.setUsuario(usuario);
			input.setFileExtension(fileExtension);
			input.setFeMessage(feMessage);
			return input;
		}
	}

}