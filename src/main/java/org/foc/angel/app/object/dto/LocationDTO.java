package org.foc.angel.app.object.dto;

import java.io.IOException;
import java.io.Serializable;

import org.foc.angel.app.annotations.JSONQuery;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.utils.Constantes;

/**
 * Objeto de tipo {@link MongoDTOObject} que contiene los datos de la
 * localizacion
 * */
@JSONQuery(nameQuery = Constantes.QUERY_GEO, query = " {posicion  : { $near : [?,?] , $maxDistance : ? } } ")
public class LocationDTO implements MongoDTOObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2391137103500774057L;

	private Double radius;
	private Double positionX;
	private Double positionY;

	public Double getRadius() {
		return radius;
	}

	public void setRadius(Double radius) {
		this.radius = radius;
	}

	public Double getPositionX() {
		return positionX;
	}

	public void setPositionX(Double positionX) {
		this.positionX = positionX;
	}

	public Double getPositionY() {
		return positionY;
	}

	public void setPositionY(Double positionY) {
		this.positionY = positionY;
	}

	@Override
	public String toJSON() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MongoDTOObject toObject(String JSONOrigen) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((positionX == null) ? 0 : positionX.hashCode());
		result = prime * result
				+ ((positionY == null) ? 0 : positionY.hashCode());
		result = prime * result + ((radius == null) ? 0 : radius.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LocationDTO other = (LocationDTO) obj;
		if (positionX == null) {
			if (other.positionX != null) {
				return false;
			}
		} else if (!positionX.equals(other.positionX)) {
			return false;
		}
		if (positionY == null) {
			if (other.positionY != null) {
				return false;
			}
		} else if (!positionY.equals(other.positionY)) {
			return false;
		}
		if (radius == null) {
			if (other.radius != null) {
				return false;
			}
		} else if (!radius.equals(other.radius)) {
			return false;
		}
		return true;
	}

	public static class Builder {

		private Double radius;
		private Double positionX;
		private Double positionY;

		public Builder radius(Double radius) {
			this.radius = radius;
			return this;
		}

		public Builder positionX(Double positionX) {
			this.positionX = positionX;
			return this;
		}

		public Builder positionY(Double positionY) {
			this.positionY = positionY;
			return this;
		}

		public LocationDTO build() {
			LocationDTO locationDTO = new LocationDTO();
			locationDTO.setPositionX(positionX);
			locationDTO.setPositionY(positionY);
			locationDTO.setRadius(radius);
			return locationDTO;
		}
	}
}