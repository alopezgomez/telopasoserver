package org.foc.angel.app.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.foc.angel.app.annotations.Projection;
import org.foc.angel.app.object.MongoDTOObject;

import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

/**
 * Clase de utilidad que crea projections para las querys en mongodb para los
 * metodos anotados con {@link Projection}
 * 
 * @author angel
 * */
public final class ProjectionUtils {

	private static final String VALUE_PROJECTION = ":'1'";

	private ProjectionUtils() {
	}

	public static <T extends MongoDTOObject> BasicDBObject projection(
			T dtoObject) throws Exception {

		Map<String, Object> mapDataProjection = makeProjection(dtoObject);
		String projection = projectionMaked(mapDataProjection, Constantes.EMPTY);
		String projectionJSONFormat = toJSONFormat(projection);
		return (BasicDBObject) JSON.parse(projectionJSONFormat);

	}

	private static String toJSONFormat(String projection) {
		String[] dataProjection = projection.split(Constantes.LINE_FEED);
		String projectionParsed = Constantes.LEFT_CURLY_BRACKET;
		for (int i = 0; i < dataProjection.length; i++) {
			if (i == dataProjection.length - 1) {
				projectionParsed += dataProjection[i];
			} else {
				projectionParsed += dataProjection[i] + Constantes.COMMA;
			}
		}
		projectionParsed += Constantes.RIGHT_CURLY_BRACKET;
		return projectionParsed;
	}

	private static String projectionMaked(Map<String, Object> mapaProjections,
			String padre) {

		Set<Entry<String, Object>> entrySet = mapaProjections.entrySet();
		String cadena = Constantes.EMPTY;
		if (StringUtils.isBlank(padre)) {
			for (Entry<String, Object> entry : entrySet) {
				if (entry.getValue() instanceof HashMap<?, ?>) {
					cadena += projectionMaked(
							(Map<String, Object>) entry.getValue(),
							entry.getKey());
				} else {
					cadena += entry.getKey() + VALUE_PROJECTION
							+ Constantes.LINE_FEED;
				}
			}
		} else {
			for (Entry<String, Object> entry : entrySet) {
				if (entry.getValue() instanceof HashMap<?, ?>) {
					cadena += projectionMaked(
							(Map<String, Object>) entry.getValue(), padre
									+ Constantes.DOT + entry.getKey());
				} else {
					cadena += padre + Constantes.DOT + entry.getKey()
							+ VALUE_PROJECTION + Constantes.LINE_FEED;
				}
			}
		}

		return cadena;
	}

	private static <T extends MongoDTOObject> Map<String, Object> makeProjection(
			T dtoObject) throws Exception {

		Method[] methods = dtoObject.getClass().getMethods();
		Map<String, Object> mapa = new HashMap<String, Object>();
		for (Method method : methods) {
			String nomMethod = method.getName();
			if (method.isAnnotationPresent(Projection.class)) {
				Projection projection = method.getAnnotation(Projection.class);
				Object invoke = method.invoke(dtoObject, new Object[] {});
				if (invoke != null) {
					String nomAttr = generateNameAttr(nomMethod);
					switch (projection.value()) {
					case LIST:
						for (Object object : (ArrayList) invoke) {
							mapa.put(nomAttr,
									makeProjection((MongoDTOObject) object));
						}
						break;
					case SIMPLE:
						mapa.put(nomAttr, Constantes.VALUE_PROJECTION);
						break;
					case TYPE:
						mapa.put(nomAttr,
								makeProjection((MongoDTOObject) invoke));
						break;

					}
				}
			}

		}

		return mapa;
	}

	private static String generateNameAttr(String nameMethod) {
		String nomAttr = nameMethod.substring(4);
		String fistChar = String.valueOf(nameMethod.substring(3).charAt(0))
				.toLowerCase();
		return fistChar + nomAttr;
	}

}
