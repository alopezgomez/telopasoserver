package org.foc.angel.app.utils;

public final class Constantes {

	private Constantes() {

	}

	public static final String EMPTY = "";
	public static final String RIGHT_CURLY_BRACKET = "}";
	public static final String LEFT_CURLY_BRACKET = "{";
	public static final String COMMA = ",";
	public static final String DOT = ".";
	public static final String LINE_FEED = "\n";
	public static final String YES = "Y";

	public static final String VALUE_PROJECTION = "1";
	public static final String OPERATOR_PUSH = "$push";
	public static final String OPERATOR_UNSET = "$unset";
	public static final String OPERATOR_PULL = "$pull";

	public static final String QUERY_ID_GENERADO = "queryIdGenerado";
	public static final String QUERY_USER = "queryUser";
	public static final String QUERY_PICKED = "queryPicked";
	public static final String QUERY_FOR_MESSAGE = "queryForMessage";
	public static final String SEQUENCES = "sequence";
	public static final String QUERY_MESSAGES = "queryMessage";
	public static final String QUERY_ALL_MESSAGE = "queryAllMessage";
	public static final String QUERY_ELEM_MATCH_USERNAME = "queryElemMathListUserName";
	public static final String QUERY_ELEM_MATCH = "queryElemMathListUser";
	public static final String QUERY_ADMIN_USER = "queryUserAdminName";
	public static final String QUERY_GEO = "geoEspacial";
	public static final double KILOMETROS_RADIANES = 111.120;

}
