package org.foc.angel.app.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anotacion que indica sobre que coleccion trabaja cada objeto de tipo
 * {@link MongoDTOObject}
 * 
 * @author angel
 * */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CollectionName {

	/**
	 * Indica el nombre de la coleccion de la cual es responsable el objeto
	 * anotado
	 * */
	String value() default "";

}
