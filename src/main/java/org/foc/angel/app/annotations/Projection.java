package org.foc.angel.app.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.foc.angel.app.enums.ProyectionType;

/**
 * Anotacion que permite marcar los campos para realizar la proyecci�n para
 * las consultas en mongoDB
 * 
 * @author angel
 * */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Projection {

	ProyectionType value() default ProyectionType.SIMPLE;

}
