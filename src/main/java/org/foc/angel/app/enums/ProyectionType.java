package org.foc.angel.app.enums;

/**
 * Enumeracion que contiene los tipos de proyecciones utilizadas en la anotacion
 * {@link org.foc.angel.app.annotations.Projection}
 * 
 * @author angel
 * */
public enum ProyectionType {

	TYPE, LIST, SIMPLE

}
