package org.foc.angel.app.factory;

/**
 * Enumeración que hace las funciones de factoria de creacion de objetos para
 * los adapters de los mensajes
 * 
 * @author angel
 * */
public enum AdaptersFactory {

	AdapterMessages() {
		@Override
		public Adapter getAdapter() {
			return new AdapterMessage();
		}

	},

	AdapterImages() {
		@Override
		public Adapter getAdapter() {
			return new AdpaterImage();
		}

	},
	AdpaterLocations() {
		@Override
		public Adapter getAdapter() {
			return new AdapterLocation();
		}

	};

	public abstract Adapter getAdapter();

}
