package org.foc.angel.app.factory;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.foc.angel.app.object.InputObject;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.ImageDTO;
import org.foc.angel.app.object.input.MessageInput;

/**
 * Adaptador para la transformacion del un objeto {@link MessageInput} a un
 * objeto {@link ImageDTO} y viceversa
 * */
public class AdpaterImage implements Adapter {

	private static Map<String, String> contentTypes = new HashMap<>();
	static {
		contentTypes.put(".bmp", "image/bmp");
		contentTypes.put(".jpeg", "image/jpeg");
		contentTypes.put(".jpg", "image/jpeg");
		contentTypes.put(".gif", "image/gif");
		contentTypes.put(".png", "image/png");

	}

	@Override
	public InputObject mongoDTOToInputObject(MongoDTOObject mongoDTOObject) {
		MessageInput messageInput = new MessageInput();
		ImageDTO imageDTO = (ImageDTO) mongoDTOObject;
		messageInput.setFileUpload(new String(Base64.encodeBase64(imageDTO
				.getContentOut().toByteArray())));
		Set<Entry<String, String>> entrySet = contentTypes.entrySet();
		for (Entry<String, String> entry : entrySet) {
			if (entry.getValue().equals(imageDTO.getContentType())) {
				messageInput.setFileExtension(entry.getKey());
				break;
			}
		}

		return messageInput;
	}

	@Override
	public MongoDTOObject inputObjectToMongoDTO(InputObject inputObject) {

		MessageInput messageInput = (MessageInput) inputObject;

		byte[] decodeContent = Base64
				.decodeBase64(messageInput.getFileUpload());
		ByteArrayInputStream contentIn = new ByteArrayInputStream(decodeContent);

		ImageDTO imageDTO = new ImageDTO.Builder().content(contentIn)
				.idMessage(messageInput.getIdMessage())
				.contentType(contentTypes.get(messageInput.getFileExtension()))
				.build();

		return imageDTO;
	}
}
