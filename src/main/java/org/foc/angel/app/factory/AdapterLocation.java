package org.foc.angel.app.factory;

import org.foc.angel.app.object.InputObject;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.LocationDTO;
import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.utils.Constantes;

/**
 * Adaptador para la conversion de un objeto {@link LocationInput} a un objeto
 * {@link LocationDTO}
 * */
public class AdapterLocation implements Adapter {

	@Override
	public InputObject mongoDTOToInputObject(MongoDTOObject mongoDTOObject) {
		return null;
	}

	@Override
	public MongoDTOObject inputObjectToMongoDTO(InputObject inputObject) {

		LocationInput input = (LocationInput) inputObject;
		Double radius = Double.parseDouble(input.getRadio())
				/ Constantes.KILOMETROS_RADIANES;
		return new LocationDTO.Builder().radius(radius)
				.positionX(Double.valueOf(input.getPosicion()[0]))
				.positionY(Double.valueOf(input.getPosicion()[1])).build();
	}
}
