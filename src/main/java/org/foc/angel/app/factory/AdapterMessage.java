package org.foc.angel.app.factory;

import org.foc.angel.app.object.InputObject;
import org.foc.angel.app.object.MongoDTOObject;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.MessageInput;

/**
 * Implementacion de la interfaz {@link Adapter} para adaptar los objetos de
 * tipo {@link InputObject} y {@link MongoDTOObject}
 * 
 * @author angel
 * */
public class AdapterMessage implements Adapter {

	@Override
	public InputObject mongoDTOToInputObject(MongoDTOObject mongoDTOObject) {

		MessageDTO messageDTO = (MessageDTO) mongoDTOObject;
		MessageInput messageInput = new MessageInput.Build()
				.id(messageDTO.getIdGenerado())
				.date(messageDTO.getFechaCreacion())
				.categoria(messageDTO.getCategoria())
				.descripcion(messageDTO.getDescripcion())
				.titulo(messageDTO.getTitulo())
				.usuario(messageDTO.getUsuario()).build();

		if (hayPosicion(messageDTO)) {
			messageInput.setPosicion(new String[] {
					String.valueOf(messageDTO.getPosicion()[0]),
					String.valueOf(messageDTO.getPosicion()[1]) });
		}

		return messageInput;
	}

	private boolean hayPosicion(MessageDTO messageDTO) {
		return messageDTO.getPosicion() != null
				&& messageDTO.getPosicion().length == 2;
	}

	@Override
	public MongoDTOObject inputObjectToMongoDTO(InputObject inputObject) {

		MessageInput messageInput = (MessageInput) inputObject;
		String[] posicion = messageInput.getPosicion();
		Double[] positions = null;
		if (posicion != null) {
			positions = new Double[] { Double.parseDouble(posicion[0]),
					Double.parseDouble(posicion[1]) };
		}
		MessageDTO messageDTO = new MessageDTO.Builder()
				.idGenerado(messageInput.getIdMessage())
				.categoria(messageInput.getCategoria())
				.descripcion(messageInput.getDescripcion()).posicion(positions)
				.titulo(messageInput.getTitulo())
				.usuario(messageInput.getUsuario()).build();

		return messageDTO;
	}
}
