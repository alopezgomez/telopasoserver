package org.foc.angel.app.factory;

import org.foc.angel.app.object.InputObject;
import org.foc.angel.app.object.MongoDTOObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Interfaz contrato para los objetos para del adapter de objetos
 * 
 * @author angel
 * */
public interface Adapter {

	final Logger log = LoggerFactory.getLogger(Adapter.class);

	/**
	 * Metodo que realiza la transformacion de un objeto {@link MongoDTOObject}
	 * a un {@link InputObject}
	 * 
	 * @return {@link InputObject}
	 * */
	InputObject mongoDTOToInputObject(MongoDTOObject mongoDTOObject);

	/**
	 * Metodo que realiza la adaptacion de un objeto {@link InputObject} a un
	 * {@link MongoDTOObject}
	 * 
	 * @return {@link MongoDTOObject}
	 * */
	MongoDTOObject inputObjectToMongoDTO(InputObject inputObject);

}
