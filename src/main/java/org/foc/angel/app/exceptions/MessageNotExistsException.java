package org.foc.angel.app.exceptions;

/**
 * Checked exception para el control de la existencia de los mensajes en el
 * sistema
 * 
 * @author angel
 * */
public class MessageNotExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8065342286494622114L;

	public MessageNotExistsException() {

	}

	public MessageNotExistsException(String arg0) {
		super(arg0);

	}

	public MessageNotExistsException(Throwable arg0) {
		super(arg0);

	}

	public MessageNotExistsException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	public MessageNotExistsException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

	@Override
	public String getMessage() {
		return "No hay datos para ese mensaje";
	}

}
