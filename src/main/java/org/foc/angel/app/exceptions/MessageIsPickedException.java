package org.foc.angel.app.exceptions;

/**
 * Excepcion customizada para el envio de mensajes erroneos cuando un envio ya
 * ha sido recogido y se solicita de nuevo
 * */
public class MessageIsPickedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1670178348076806457L;

	public MessageIsPickedException() {
		super();
	}

	public MessageIsPickedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MessageIsPickedException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageIsPickedException(String message) {
		super(message);
	}

	public MessageIsPickedException(Throwable cause) {
		super(cause);
	}

	@Override
	public String getMessage() {
		return "Ya ha sido cogido!!";
	}

}
