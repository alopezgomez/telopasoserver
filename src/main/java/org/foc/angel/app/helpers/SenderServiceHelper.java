package org.foc.angel.app.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.foc.angel.app.factory.Adapter;
import org.foc.angel.app.factory.AdaptersFactory;
import org.foc.angel.app.object.dto.LocationDTO;
import org.foc.angel.app.object.dto.MessageDTO;
import org.foc.angel.app.object.input.LocationInput;
import org.foc.angel.app.object.input.MessageInput;
import org.foc.angel.app.utils.Constantes;
import org.foc.angel.app.utils.ProjectionUtils;
import org.foc.angel.app.utils.QueryUtils;

import com.mongodb.BasicDBObject;

/**
 * Clase helper para el servicio
 * {@link org.foc.angel.app.services.SenderService} Contiene los metodos de
 * transformacion de datos para las acciones del servicio
 * */
public final class SenderServiceHelper {

	public List<MessageInput> transformResult(List<MessageDTO> listResult,
			String userLocation) {
		Adapter messageAdapter = AdaptersFactory.AdapterMessages.getAdapter();
		List<MessageInput> listReturn = new ArrayList<>();
		for (MessageDTO message : listResult) {
			MessageInput input = (MessageInput) messageAdapter
					.mongoDTOToInputObject(message);
			if (message.getIsPicked() == null
					&& !Arrays.asList(
							message.getUsersRead() != null ? message
									.getUsersRead() : new String[] {})
							.contains(userLocation)) {
				listReturn.add(input);
			}

		}
		return listReturn;
	}

	public List<MessageInput> transformResultForPicked(
			List<MessageDTO> listResult, String userLocation) {
		Adapter messageAdapter = AdaptersFactory.AdapterMessages.getAdapter();
		List<MessageInput> listReturn = new ArrayList<>();
		for (MessageDTO message : listResult) {
			MessageInput input = (MessageInput) messageAdapter
					.mongoDTOToInputObject(message);
			if (message.getIsPicked() != null
					&& userLocation.equals(message.getIsPicked())) {
				listReturn.add(input);
			}

		}
		return listReturn;
	}

	public BasicDBObject prepareProjection() throws Exception {
		MessageDTO messageProjection = new MessageDTO.Builder()
				.idGenerado(Constantes.VALUE_PROJECTION)
				.titulo(Constantes.VALUE_PROJECTION)
				.descripcion(Constantes.VALUE_PROJECTION).build();
		BasicDBObject projection = ProjectionUtils
				.projection(messageProjection);
		return projection;
	}

	public BasicDBObject prepareProjectionAllMessages() throws Exception {
		MessageDTO messageProjection = new MessageDTO.Builder()
				.idGenerado(Constantes.VALUE_PROJECTION)
				.titulo(Constantes.VALUE_PROJECTION)
				.feCreacion(Constantes.VALUE_PROJECTION)
				.categoria(Constantes.VALUE_PROJECTION)
				.picked(Constantes.VALUE_PROJECTION)
				.usuario(Constantes.VALUE_PROJECTION)
				.posicion(new Double[] { 2D })
				.usersRead(new String[] { Constantes.VALUE_PROJECTION })
				.descripcion(Constantes.VALUE_PROJECTION).build();
		BasicDBObject projection = ProjectionUtils
				.projection(messageProjection);
		return projection;
	}

	public String prepareQuery(LocationInput locationInput) {
		Adapter locationAdapter = AdaptersFactory.AdpaterLocations.getAdapter();
		LocationDTO locationDTO = (LocationDTO) locationAdapter
				.inputObjectToMongoDTO(locationInput);
		Object[] parms = new Object[] { locationDTO.getPositionX(),
				locationDTO.getPositionY(), locationDTO.getRadius() };

		String jsonQuery = QueryUtils.getJSONQuery(Constantes.QUERY_GEO,
				LocationDTO.class, parms);
		return jsonQuery;
	}

}
