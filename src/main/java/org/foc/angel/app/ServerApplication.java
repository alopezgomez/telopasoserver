package org.foc.angel.app;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Punto de inicio de los servicios rest del servidor
 * */
@ApplicationPath("api")
public class ServerApplication extends Application {

}
